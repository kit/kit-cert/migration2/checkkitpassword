module checkkitpassword

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/fatih/color v1.10.0
	github.com/go-ldap/ldap/v3 v3.2.4
	github.com/k0kubun/pp v3.0.1+incompatible // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
)
