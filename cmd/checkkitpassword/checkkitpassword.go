package main

import (
	"bufio"
	"crypto/tls"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/pkg/errors"

	"github.com/BurntSushi/toml"
	"github.com/fatih/color"
	ldap "github.com/go-ldap/ldap/v3"
	"github.com/mitchellh/go-homedir"
)

const (
	LDAPFilterTemplate = `(|(sAMAccountName=%s)(proxyAddresses=smtp:%s*)(mail=%s*)(userPrincipalName=%s*))`
)

var (
	LDAPFilter = func(query string) string {
		escaped := ldap.EscapeFilter(query)
		return fmt.Sprintf(LDAPFilterTemplate, escaped, escaped, escaped, escaped)
	}
	wantedLDAPAttributes = []string{"distinguishedName", "cn", "description", "department", "displayName",
		"extensionAttribute9", "givenName", "lastLogon", "physicalDeliveryOfficeName", "proxyAddresses",
		"sAMAccountName", "sn", "submissionContLength", "targetAddress", "telephoneNumber", "userAccountControl",
		"userPrincipalName"}
	config struct {
		Delimiter DelimiterValue
		LDAP      LDAPConfig
	}
)

type LDAPConfig struct {
	Host           string
	Port           int
	BaseDN         string
	FilterTemplate string
	User           string
	Pass           string
	PassCmd        []string
}

type DelimiterValue struct {
	Delimiter rune
}

func (d DelimiterValue) String() string {
	return string(d.Delimiter)
}

func (d DelimiterValue) Set(s string) error {
	runes := []rune(s)
	switch len(runes) {
	case 0:
		return errors.New("delimiter string is empty")
	case 1:
		d.Delimiter = runes[0]
		return nil
	default:
		return errors.New("delimiter string contains more than one rune")
	}
}

// ParseEntry extracts certain attributes from a single LDAP search result into a map[string]interface{}
func ParseEntry(e *ldap.Entry) map[string]interface{} {
	r := make(map[string]interface{})
	r["dn"] = e.DN
	for _, key := range wantedLDAPAttributes {
		r[key] = e.GetAttributeValue(key)
	}
	for _, key := range []string{"memberOf", "proxyAddresses", "userCertificate"} {
		r[key] = e.GetAttributeValues(key)
	}
	return r
}

func init() {
	var (
		err         error
		configfile  string
		configfiles = []string{"/etc/checkkitpassword.toml", "~/.config/checkkitpassword.toml"}
	)

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	config.Delimiter.Delimiter = ':'
	flag.Var(config.Delimiter, "d", "CSV field delimiter")

	flag.Parse()

	if configfile != "" {
		configfiles = []string{configfile}
	}

	// expand ~ if present
	for idx, c := range configfiles {
		expandedFilename, err := homedir.Expand(c)
		if err != nil {
			log.Printf("Unable to expand config filename: %s", err)
		}
		configfiles[idx] = expandedFilename
	}

	// read configuration files
	var numConfigreadErrors []error
	for _, c := range configfiles {
		if _, err = toml.DecodeFile(c, &config); err != nil {
			numConfigreadErrors = append(numConfigreadErrors, err)
		}
	}

	if len(numConfigreadErrors) >= len(configfiles) {
		log.Println("Unable to read config files:")
		for _, err := range numConfigreadErrors {
			log.Println(err)
		}
		os.Exit(1)
	}

	if len(config.LDAP.PassCmd) > 0 {
		cmdpath, err := exec.LookPath(config.LDAP.PassCmd[0])
		if err != nil {
			log.Fatalf("Can't find executable %s from %s: %s\n", config.LDAP.PassCmd[0], color.RedString("PassCmd"), err)
		}
		cmd := exec.Command(cmdpath, config.LDAP.PassCmd[1:]...)
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			log.Fatal(err)
		}
		if err := cmd.Start(); err != nil {
			log.Fatal(err)
		}
		pass, err := bufio.NewReader(stdout).ReadString('\n')
		if err != nil {
			log.Fatalf("Unable to read stdout from PassCmd: %s", err)
		}
		if err := cmd.Wait(); err != nil {
			log.Fatal(err)
		}
		config.LDAP.Pass = strings.TrimSuffix(pass, "\n")
	}
}

type RawCredential struct {
	Username string
	Password string
}

func main() {
	var (
		input    *bufio.Reader
		leakdata []RawCredential
	)

	// open input
	argv := flag.Args()
	if len(argv) == 0 || argv[0] == "-" {
		input = bufio.NewReader(os.Stdin)
	} else {
		fd, err := os.Open(argv[0])
		if err != nil {
			log.Panic(err)
		}
		input = bufio.NewReader(fd)
	}

	// read input
	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		line := scanner.Text()
		token := strings.SplitN(line, string(config.Delimiter.Delimiter), 2)
		if len(token) >= 2 {
			user := strings.TrimSpace(token[0])
			pass := token[1]
			leakdata = append(leakdata, RawCredential{user, pass})
		}

	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}

	// connect to LDAP
	connstring := fmt.Sprintf("ldaps://%s:%d", config.LDAP.Host, config.LDAP.Port)
	tlsconfig := ldap.DialWithTLSConfig(&tls.Config{
		ServerName: config.LDAP.Host,
	})
	conn, err := ldap.DialURL(connstring, tlsconfig)
	if err != nil {
		log.Fatal(err)
	}
	// bind to LDAP
	err = conn.Bind(config.LDAP.User, config.LDAP.Pass)
	if err != nil {
		log.Fatal(err)
	}

	for _, credential := range leakdata {
		// prepare search (find credential)
		query := ldap.NewSearchRequest(
			config.LDAP.BaseDN,
			ldap.ScopeWholeSubtree,
			ldap.DerefAlways,
			0, 0, false,
			LDAPFilter(credential.Username),
			wantedLDAPAttributes,
			nil)

		// search LDAP (find credential)
		searchresults, err := conn.Search(query)
		if err != nil {
			log.Fatal(err)
		}

		type CredentialMatches struct {
			LDAPData     map[string]interface{}
			Password     string
			LogonSuccess bool
		}
		//pp.Print(searchresults)

		var (
			matches      []CredentialMatches
			LDAPTestConn *ldap.Conn
		)

		for _, r := range searchresults.Entries {
			entry := ParseEntry(r)
			LDAPTestConn, err = ldap.DialURL(connstring, tlsconfig)
			if err != nil {
				log.Fatal(err)
			}
			err = LDAPTestConn.Bind(r.DN, credential.Password)
			// password working: yes
			if err == nil {
				fmt.Printf("🛑 %s\n", credential.Username)
				matches = append(matches, CredentialMatches{
					LDAPData:     entry,
					Password:     credential.Password,
					LogonSuccess: true,
				})
				// no
			} else {
				//fmt.Printf("👍 %s\n", credential.Username)
			}
			LDAPTestConn.Close()
		}

		//pp.Println(matches)
	}
}
